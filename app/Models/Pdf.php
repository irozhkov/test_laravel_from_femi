<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class Pdf extends Model
{

    protected $fillable = [
        'name',
        'path',
        'size'

    ];

    static public function saveFile(UploadedFile $file)
    {
        $name = str_replace('.pdf','',$file->getClientOriginalName());
        $size = $file->getSize();
        $path = $file->getBasename().'.pdf';
        $pdf = self::where('name','=',$name)->where('size','=',$size)->first();

        if(empty($pdf)){
            $file->move(storage_path("app/public/uploads/"),$path);

            self::create([
                'name' => $name,
                'path'=>$path,
                'size' => $size,
            ]);
        }else{
            $file->move(storage_path("app/public/uploads/"),$path);
        }


    }
}
