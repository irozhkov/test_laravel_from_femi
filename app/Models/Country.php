<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'contry_id',
    ];

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
}
