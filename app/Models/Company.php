<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'country_id',
        'name',
        'id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
