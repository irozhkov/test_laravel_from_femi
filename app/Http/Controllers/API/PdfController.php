<?php

namespace App\Http\Controllers\API;

use App\Models\Pdf;
use App\PdfService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PdfController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        if ($file->getClientOriginalExtension() !== 'pdf')
            return response([
                'status' => 'error',
                'data' => 'File is not pdf'
            ], 422);



        if(PdfService::isSearchFor($file,'text')){
            Pdf::saveFile($file);
        }

        return response([
            'status' => 'success',
            'data' => 'true'
        ], 200);

    }
}
