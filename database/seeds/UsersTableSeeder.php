<?php

use Illuminate\Database\Seeder;

use App\Models\Company;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countryIBM = Company::where('name', 'IBM')->first();
        $countryAMD = Company::where('name', 'AMD')->first();
        $countryGoogle = Company::where('name', 'Google')->first();

	    $user1 = new User();
	    $user1->name = 'User1';
	    $user1->email = 'User1@no-spam.ws';
	    $user1->password = bcrypt('123456');
	    $user1->save();
	    $user1->companies()->attach($countryIBM);

	    $user2 = new User();
	    $user2->name = 'User2';
	    $user2->email = 'User2@no-spam.ws';
	    $user2->password = bcrypt('123456');	    
	    $user2->save();
	    $user2->companies()->attach($countryIBM);
	    $user2->companies()->attach($countryAMD);
	    $user2->companies()->attach($countryGoogle);

	    $user3 = new User();
	    $user3->name = 'User3';
	    $user3->email = 'User3@no-spam.ws';
	    $user3->password = bcrypt('123456');	    
	    $user3->save();
	    $user3->companies()->attach($countryAMD);

	    $user4 = new User();
	    $user4->name = 'User4';
	    $user4->email = 'User4@no-spam.ws';
	    $user4->password = bcrypt('123456');	    
	    $user4->save();
	    $user4->companies()->attach($countryAMD);

	    $user5 = new User();
	    $user5->name = 'User5';
	    $user5->email = 'User5@no-spam.ws';
	    $user5->password = bcrypt('123456');	    
	    $user5->save();
	    $user5->companies()->attach($countryGoogle);

    }
}
