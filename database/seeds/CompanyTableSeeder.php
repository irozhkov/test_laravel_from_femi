<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
                ['id' => 1, 'name' => 'IBM', 'country_id' => 1],
                ['id' => 2, 'name' => 'AMD', 'country_id' => 1],
                ['id' => 3, 'name' => 'Google', 'country_id' => 2],
            ]
        );
    }
}
